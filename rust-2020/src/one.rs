use crate::util::{IsComplete, ProcessInput, Puzzle};
use itertools::Itertools;

pub struct One {
    all_values: Vec<i32>,
    pub puzzle: Puzzle,
}
pub struct One2 {
    all_values: Vec<i32>,
    pub puzzle: Puzzle,
}

impl One {
    #[allow(unused)]
    pub fn new() -> One {
        let mut o = One {
            all_values: Vec::new(),
            puzzle: Puzzle::new("One1".to_string()),
        };
        o.puzzle.filename = String::from("one.txt");
        o
    }
}

impl One2 {
    #[allow(unused)]
    pub fn new() -> One2 {
        let mut o = One2 {
            all_values: Vec::new(),
            puzzle: Puzzle::new("One2".to_string()),
        };
        o.puzzle.filename = String::from("one.txt");
        o
    }
}

impl ProcessInput for One {
    fn process_input(&mut self, line: String) -> bool {
        let i = line.parse::<i32>().unwrap();
        for v in &self.all_values {
            if 2020 == *v + i {
                self.puzzle
                    .set_answer(format!("{} and {} = {}", *v, i, *v * i));
                self.puzzle.set_done(true);
                break;
            }
        }
        self.all_values.push(i);
        self.puzzle.is_complete()
    }
}

impl ProcessInput for One2 {
    fn process_input(&mut self, line: String) -> bool {
        let i = line.parse::<i32>().unwrap();
        let all_comb = self.all_values.iter().combinations(2);
        for v in all_comb {
            if 2020 == v.iter().copied().sum::<i32>() + i {
                self.puzzle
                    .set_answer(format!("3 combined = {}", v.iter().copied().product::<i32>() * i));
                self.puzzle.set_done(true);
                break;
            }
        }
        self.all_values.push(i);
        self.puzzle.is_complete()
    }
}
