use crate::util::{CompleteCallback, ProcessInput, Puzzle};

pub struct Three {
    checker: Checker,
    pub puzzle: Puzzle,
}

impl Three {
    #[allow(unused)]
    pub fn new() -> Three {
        let mut o = Three {
            checker: Checker::new(3, 1),
            puzzle: Puzzle::new("Three".to_string()),
        };
        o.puzzle.filename = String::from("three.txt");
        o
    }
}

impl Three2 {
    #[allow(unused)]
    pub fn new() -> Three2 {
        Three2 {
            puzzle: Puzzle::newf("Three2".to_string(), "rob3.txt".to_string()),
            // Right 1, down 1.
            // Right 3, down 1. (This is the slope you already checked.)
            // Right 5, down 1.
            // Right 7, down 1.
            // Right 1, down 2.
            checkers: vec![
                Checker::new(1, 1),
                Checker::new(3, 1),
                Checker::new(5, 1),
                Checker::new(7, 1),
                Checker::new(1, 2),
            ],
        }
    }
}

pub struct Three2 {
    pub puzzle: Puzzle,
    checkers: Vec<Checker>,
}
impl CompleteCallback for Three2 {
    fn complete(&mut self) {}
}
impl CompleteCallback for Three {
    fn complete(&mut self) {}
}
struct Checker {
    pub count: usize,
    cur_pos: usize,
    h_delta: usize,
    v_delta: usize,
    rows_skipped: usize,
}
impl Checker {
    fn new(hd: usize, vd: usize) -> Checker {
        Checker {
            count: 0,
            cur_pos: 0,
            h_delta: hd,
            v_delta: vd,
            rows_skipped: vd - 1,
        }
    }

    fn check_line(&mut self, line: &String) {
        if self.rows_skipped == self.v_delta - 1 {
            if line.chars().nth(self.cur_pos).unwrap() == '#' {
                self.count += 1;
            }
            self.cur_pos = (self.cur_pos + self.h_delta) % line.len();
            self.rows_skipped = 0;
        } else {
            self.rows_skipped += 1;
        }
    }
}

impl ProcessInput for Three {
    fn process_input(&mut self, line: String) -> bool {
        self.checker.check_line(&line);
        // println!("{}, {}, {}, {}", self.count, line, line.len(), self.cur_pos);
        self.puzzle
            .set_answer(format!("count = {}", self.checker.count));
        false
    }
}

impl ProcessInput for Three2 {
    fn process_input(&mut self, line: String) -> bool {
        for ch in self.checkers.iter_mut() {
            ch.check_line(&line);
        }
        // println!("{}, {}, {}, {}", self.count, line, line.len(), self.cur_pos);
        let mut ans: String = String::new();
        let mut prod: usize = 1;
        for ch in self.checkers.iter() {
            ans.push_str(&format!("count = {}\n", ch.count));
            prod *= ch.count;
        }
        self.puzzle.set_answer(format!("product = {}\n", prod));
        self.puzzle.append_answer(ans);
        false
    }
}
