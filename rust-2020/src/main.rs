use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;

mod four;
mod one;
mod three;
mod two;
mod util;

use util::{CompleteCallback, PrintAnswer, ProcessInput};


fn main() -> io::Result<()> {
    // let dir = std::fs::read_dir("..")?;
    // for d in dir {
    //     println!("{}", d?.path().display());
    // }
    let mut solver = four::Four::part1();
    println!("Running for day {}", solver.puzzle.name);
    let f = File::open(format!("download/{}", solver.puzzle.filename))?;
    let reader = BufReader::new(f);
    for line in reader.lines() {
        if solver.process_input(line?) {
            break;
        }
    }
    solver.complete();
    solver.puzzle.set_done(true);
    solver.puzzle.print_answer();
    Ok(())
}
