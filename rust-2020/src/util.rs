pub trait ProcessInput {
    fn process_input(&mut self, line: String) -> bool;
}

pub trait IsComplete {
    fn is_complete(&self) -> bool;
}

pub trait CompleteCallback {
    fn complete(&mut self) {}
}

pub trait PrintAnswer {
    fn print_answer(&self);
}

pub struct Puzzle {
    done: bool,
    answer: String,
    pub name: String,
    pub filename: String,
}

#[allow(unused)]
impl Puzzle {
    pub fn new(name: String) -> Puzzle {
        Puzzle {
            done: false,
            answer: String::new(),
            name: name,
            filename: String::new(),
        }
    }
    pub fn newf(name: String, filename: String) -> Puzzle {
        Puzzle {
            done: false,
            answer: String::new(),
            name: name,
            filename: filename,
        }
    }
    pub fn set_answer(&mut self, ans: String) {
        self.answer = ans;
    }
    pub fn append_answer(&mut self, ans: String) {
        self.answer.push_str(&ans);
    }
    pub fn set_done(&mut self, done: bool) {
        self.done = done;
    }
    pub fn print(&self) {
        println!(
            "[{}] Done: {}, Answer: {}",
            self.name, self.done, self.answer
        );
    }
}

impl IsComplete for Puzzle {
    fn is_complete(&self) -> bool {
        self.done
    }
}

impl PrintAnswer for Puzzle {
    fn print_answer(&self) {
        println!("{}", self.answer);
    }
}
