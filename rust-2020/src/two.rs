use crate::util::{ProcessInput, Puzzle};

pub struct Two {
    count_valid: i64,
    pub puzzle: Puzzle,
}
pub struct Two2 {
    count_valid: i64,
    pub puzzle: Puzzle,
}

impl Two {
    #[allow(unused)]
    pub fn new() -> Two {
        let mut o = Two {
            count_valid: 0,
            puzzle: Puzzle::new("Two".to_string()),
        };
        o.puzzle.filename = String::from("two.txt");
        o
    }
}

impl Two2 {
    #[allow(unused)]
    pub fn new() -> Two2 {
        let mut o = Two2 {
            count_valid: 0,
            puzzle: Puzzle::new("Two2".to_string()),
        };
        o.puzzle.filename = String::from("two.txt");
        o
    }
}
/// 1-3 a: abcde
/// 1-3 b: cdefg
/// 2-9 c: ccccccccc
fn get_allowable_counts(line: &String) -> (usize, usize) {
    let s = line.split(' ').nth(0);
    // println!("{:#}", line);
    let mut s2 = s.unwrap().split('-');
    (
        s2.next().unwrap().parse::<usize>().unwrap(),
        s2.next().unwrap().parse::<usize>().unwrap(),
    )
}

fn get_letter(line: &String) -> char {
    let s = line.split(' ').nth(1).unwrap();
    s.chars().next().unwrap()
}

fn get_password(line: &String) -> String {
    String::from(line.split(' ').nth(2).unwrap())
}

impl ProcessInput for Two {
    fn process_input(&mut self, line: String) -> bool {
        let allowable_counts = get_allowable_counts(&line);
        let letter = get_letter(&line);
        let password = get_password(&line);
        let count = password.matches(letter).count();
        if count >= allowable_counts.0 && count <= allowable_counts.1 {
            self.count_valid += 1;
            self.puzzle
                .set_answer(format!("count = {}", self.count_valid));
        }
        // println!(
        //     "Allowable: {}-{}, Letter: {}, password: {}, Valid: {}, self.count_valid: {}",
        //     allowable_counts.0,
        //     allowable_counts.1,
        //     letter,
        //     password,
        //     count >= allowable_counts.0 && count <= allowable_counts.1,
        //     self.count_valid
        // );
        false
    }
}

impl ProcessInput for Two2 {
    fn process_input(&mut self, line: String) -> bool {
        let allowable_inds = get_allowable_counts(&line);
        let letter = get_letter(&line);
        let password = get_password(&line);
        let inds_matches: Vec<(usize, &str)> = password.match_indices(letter).collect();
        let inds: Vec<usize> = inds_matches.into_iter().map(|(sz, _)| sz + 1).collect();
        if inds.contains(&allowable_inds.0) ^ inds.contains(&allowable_inds.1) {
            self.count_valid += 1;
            self.puzzle
                .set_answer(format!("count = {}", self.count_valid));
        }
        false
    }
}
