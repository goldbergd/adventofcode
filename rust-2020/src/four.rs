use crate::util::{CompleteCallback, ProcessInput, Puzzle};
use regex::Regex;

pub struct Four {
    cur_passport: Passport,
    part: i32,
    count_valid: usize,
    pub puzzle: Puzzle,
}

impl CompleteCallback for Four {
    fn complete(&mut self) {
        self.process_input(String::new());
    }
}

impl Four {
    #[allow(unused)]
    pub fn part1() -> Four {
        Four {
            cur_passport: Passport::new(),
            part: 1,
            count_valid: 0,
            puzzle: Puzzle::newf("Four".to_string(), "four.txt".to_string()),
        }
    }
    pub fn part2() -> Four {
        Four {
            cur_passport: Passport::new(),
            part: 2,
            count_valid: 0,
            puzzle: Puzzle::newf("Four".to_string(), "four.txt".to_string()),
        }
    }
}

struct Passport {
    fields: std::collections::HashMap<String, String>,
    required_keys: std::collections::HashMap<String, fn(String) -> bool>,
}

fn is_between(val: i32, lower: i32, upper: i32) -> bool {
    val >= lower && val <= upper
}

impl Passport {
    fn new() -> Passport {
        let mut p = Passport {
            fields: std::collections::HashMap::new(),
            required_keys: std::collections::HashMap::new(),
        };
        /* byr (Birth Year) - four digits; at least 1920 and at most 2002.
        iyr (Issue Year) - four digits; at least 2010 and at most 2020.
        eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
        hgt (Height) - a number followed by either cm or in:
        If cm, the number must be at least 150 and at most 193.
        If in, the number must be at least 59 and at most 76.
        hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
        ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
        pid (Passport ID) - a nine-digit number, including leading zeroes. */
        p.required_keys.insert("byr".to_string(), |val: String| {
            val.len() == 4 && is_between(val.parse::<i32>().unwrap(), 1920, 2002)
        });
        p.required_keys.insert("iyr".to_string(), |val: String| {
            val.len() == 4 && is_between(val.parse::<i32>().unwrap(), 2010, 2020)
        });
        p.required_keys.insert("eyr".to_string(), |val: String| {
            val.len() == 4 && is_between(val.parse::<i32>().unwrap(), 2020, 2030)
        });
        p.required_keys.insert("hgt".to_string(), |val: String| {
            (val.ends_with("cm")
                && is_between(*(&val[..(val.len() - 2)].parse::<i32>().unwrap()), 150, 193))
                || (val.ends_with("in")
                    && is_between(*(&val[..(val.len() - 2)].parse::<i32>().unwrap()), 59, 76))
        });
        p.required_keys.insert("hcl".to_string(), |val: String| {
            let re = Regex::new(r"^#[a-f0-9]{6}$").unwrap();
            re.is_match(&val)
        });
        p.required_keys.insert("ecl".to_string(), |val: String| {
            [
                "amb".to_string(),
                "blu".to_string(),
                "brn".to_string(),
                "gry".to_string(),
                "grn".to_string(),
                "hzl".to_string(),
                "oth".to_string(),
            ]
            .contains(&val)
        });
        p.required_keys.insert("pid".to_string(), |val: String| {
            let re = Regex::new(r"^[0-9]{9}$").unwrap();
            re.is_match(&val)
        });
        p
    }

    fn parse_line(&mut self, line: &String) -> bool {
        for pair in line.split_whitespace() {
            let pair_collect: Vec<&str> = pair.split(':').collect();
            self.fields
                .insert(String::from(pair_collect[0]), String::from(pair_collect[1]));
        }
        line.trim().is_empty()
    }
    fn valid(&self) -> bool {
        self.required_keys
            .keys()
            .all(|k| self.fields.contains_key(k))
    }
    fn valid2(&self) -> bool {
        self.valid()
            && self
                .fields
                .keys()
                .all(|k| self.required_keys[k](String::from(&(self.fields[k]))))
    }
}

impl ProcessInput for Four {
    fn process_input(&mut self, line: String) -> bool {
        if self.cur_passport.parse_line(&line) {
            if (self.part == 1 && self.cur_passport.valid())
                || (self.part == 2 && self.cur_passport.valid2())
            {
                self.count_valid += 1;
            }
            self.cur_passport = Passport::new();
        }
        // println!("{}, {}, {}, {}", self.count, line, line.len(), self.cur_pos);
        self.puzzle
            .set_answer(format!("count_valid = {}", self.count_valid));
        false
    }
}
